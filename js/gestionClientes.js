var clientesObtenidos;

function getClientes() {
var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var request = new XMLHttpRequest();

request.onreadystatechange = function () {

  if (this.readyState == 4 && this.status == 200) {
    console.table(JSON.parse(request.responseText).value);
    customersObteined = request.responseText;
    processCustomers();
    }
  }

  request.open("GET",url, true);
  request.send();
}

function processCustomers() {
  let urlFalg = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  let JSONCustomers = JSON.parse(customersObteined);
  //alert(JSONProductos.value[0].ProductName);
  let divTableCustomers = document.getElementById("divTableCustomers");
  let tableCustomer     = document.createElement("table");
  let tbody             = document.createElement("tbody");

  tableCustomer.classList.add("table");
  tableCustomer.classList.add("table-striped");

  for (var i = 0; i < JSONCustomers.value.length; i++) {

    let newRow = document.createElement("tr");

    let columnContactName = document.createElement("td");
    columnContactName.innerText = JSONCustomers.value[i].ContactName;

    let columnCity = document.createElement("td");
    columnCity.innerText = JSONCustomers.value[i].City;

    let columnFlag = document.createElement("td");
    let imgFlag =  document.createElement("img");
    imgFlag.classList.add("flag");

    if (JSONCustomers.value[i].Country == "UK") {
      imgFlag.src = urlFalg + "United-Kingdom.png";
    }else{
      imgFlag.src = urlFalg + JSONCustomers.value[i].Country + ".png";
    }

    columnFlag.appendChild(imgFlag);


    newRow.appendChild(columnContactName);
    newRow.appendChild(columnCity);
    newRow.appendChild(columnFlag);

    tbody.appendChild(newRow);

  }

  tableCustomer.appendChild(tbody);
  divTableCustomers.appendChild(tableCustomer);
}
